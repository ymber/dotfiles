# Matthew's dots
Intended for Arch Linux.

## Packages
Network
```
iwd
dhcpcd
openssh
```

Sound
```
pulseaudio
pulseaudio-alsa
alsa-utils
mpd
ncmpcpp
```

Utils
```
sxhkd
dunst
libnotify
maim
OOR slock-solarized-dark
neovim
AUR yay
```

Shell
```
zsh
OOR st-ymber
tmux
```

Graphical
```
openbox
sxiv
xwallpaper
arc-gtk-theme
arc-icon-theme
AUR openbox-theme-arcbox
AUR conky-lua
rofi
```

